/*2.Дан файл, содержащий русский текст, размер текста не превышает 10 К байт. Найти в тексте N (N ≤ 100) самых длинных слов, не содержащих одинаковых букв.
Записать найденные слова в текстовый файл в порядке не возрастания длины. Все найденные слова должны быть разными. Число N вводить из файла.*/
#include <iostream>
#include <fstream>
#include <sstream>
#include <windows.h>
#include <string>
using std::cout;
using std::cin;
using std::endl;
int main()
{
    setlocale(LC_ALL, "rus");
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);
    std::ifstream in("input.txt");
    std::ofstream out("output.txt");
    int N;
    in >> N;
    //Вводимое слово
    std::string word;
    //Переменная для значения длины слова
    int wordLength;
    //Массив для подсчета количества букв в слове
    int count[256];
    //Переменная обознчения невыполненного условия о неповторении букв в слове
    int wrong = 0;
    //Массив со словами, выполняющие условие о неповторении букв
    std::string trueWords[100];
    //Индекс для записи
    int ind = 0;
    while (!in.eof())
    {
        wrong = 0;
        in >> word;
        wordLength = word.length();
        //делаем все элементы нулем
        for (int i = 0; i < 256; i++)
        {
            count[i] = 0;
        }
        //считаем количество каждой встреченной буквы
        for (int i = 0; i < wordLength; i++)
        {
            //Проверка регистра
            if ((char(word[i]) >= 97 && char(word[i] <= 122)) || (char(word[i]) >= 224 && char(word[i]) <= 255))
               count[word[i] - 32] += 1;
            else
                count[word[i]] += 1;
            //если буква встретилась больше одного раза - выходим из цикла
            if (count[word[i]] > 1)
            {
                wrong = 1;
                break;
            }
        }
        if (wrong == 0)
        {
            trueWords[ind] = word;
            ind += 1;
        }

    }
    //сортировка по невозрастаню
    for (int i = 1; i < 100; i++)
    {
        if ((trueWords[i - 1].length() < trueWords[i].length()))
        {
            std::swap(trueWords[i - 1], trueWords[i]);
        }
    }
    //вывод массива
    for (int i = 0; i < 100; i++)
    {
        if (trueWords[i] != "")
        {
            cout << trueWords[i] << endl;
        }
    }
}
