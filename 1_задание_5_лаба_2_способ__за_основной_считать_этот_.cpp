#include <iostream>
#include <fstream>
#include <sstream>
#include <windows.h>
using std::cout;
using std::cin;
using std::endl;
int main()
{
	int charCount[256];
	//делаю массив, в котором все элементы - нули
	for (int i = 0; i < 256; i++) {
		charCount[i] = 0;
	}
	//N - количество символов в строке
	int N;
	cout << "How many characters must be there?" << endl;
	cin >> N;
	while (N > 100) {
		cout << "The number of characters must not exceed 100. Try again" << endl;
		cin >> N;
	}
	//a - вводимый символ
	char a;
	//вводим и считаем символы
	for (int i = 0; i < N; i++) {
		cout << "Enter " << i + 1 << " character" << endl;
		cin >> a;
		if (int(a) >= 97 && a <= 122) {
			charCount[int(a) - 32] += 1;
		}
		else
			charCount[int(a)] += 1;

	}
	// максимальный элемент массива
	int maxLis = INT_MIN;
	//индекс для таблицы аски
	int ind;
	//переменная для проверки количества максимальных символов
	int count = 1;
	//нахождение максимального элемента
	for (int i = 0; i < 256; i++) {
		if (charCount[i] > maxLis) {
			maxLis = charCount[i];
		}
		else if (maxLis == charCount[i]) {
			count += 1;
		}
	}
	//ответ при условии что есть только один символ, встречающийся максимальное количество раз
	if (count == 1) {
		for (int i = 0; i < 256; i++) {
			if (charCount[i] == maxLis) {
				ind = i;
				cout << "The most frequent character is " << '"' << char(ind) << '"' << ". It were found " << maxLis << " times" << endl;
				break;
			}
		}
	}
	//ответ при условии, что символов встречающихся максимальное количество раз несколько
	else if (count > 1) {
		cout << "The most frequent characters is:";
		for (int i = 0; i < 256; i++) {
			if (charCount[i] == maxLis) {
				ind = i;
				cout << " " << '"' << char(ind) << '"' << ", ";
			}
		}
		cout << "It were found " << maxLis << " times" << endl;
	}
}
