/*1.Дана строка длиной не более 100 символов. Найти в ней наиболее часто встречающуюся букву. Регистр не учитывать.
В условии не уточнено, что делать если разные символы встречаются одинаковое количество раз,
поэтому я решил считать за ответ тот символ который встречался чаще всего и был найден первым*/
#include <iostream>
#include <fstream>
#include <sstream>
#include <windows.h>
using std::cout;
using std::cin;
using std::endl;
int main()
{
	char lis[100];
	cout << "How many characters will be there?" << endl;
	int N;
	cin >> N;
	while (N > 100) {
		cout << "The number of charactes must not exceed 100. Try again" << endl;
		cin >> N;
	}
	char a;
	//Ввод символов
	for (int i = 0; i < N; i++) {
		cout << "Enter " << i + 1 << " character" << endl;
		cin >> a;
		if (int(a) >= 97 && int(a) <= 122)
			lis[i] = char(int(a) - 32);
		else
			lis[i] = a;
	}
	int count;
	char mostfrequent;
	int maxcount = INT_MIN;
	//Нахождение наиболее частого
	for (int i = 0; i < N; i++) {
		count = 1;
		for (int x = i + 1; x < N; x++) {
			//Подсчет того, сколько раз встречается символ lis[i]
			if (lis[x] == lis[i]) {
				count += 1;
			}

		}
		//Сравнение счетчика символа с максимальным значением счетчика
		if (count > maxcount) {
			maxcount = count;
			mostfrequent = lis[i];
		}

	}
	cout << "Character " << '"' << mostfrequent << '"' << " is most frequent. It met " << maxcount << " times" << endl;
}


